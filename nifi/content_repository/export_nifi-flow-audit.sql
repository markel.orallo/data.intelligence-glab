SET WRITE_DELAY 0;             
SET LOCK_MODE 3;               
;              
CREATE USER IF NOT EXISTS "NF" SALT '5a6037be715adba8' HASH '55e17a9455684a0e04e0540933aa8e19fb6b8f0490490100410e48dbe41f703f' ADMIN;          
CREATE SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_F490330D_9908_48AC_856B_60379B07C29C" START WITH 129 BELONGS_TO_TABLE;               
CREATE CACHED TABLE "PUBLIC"."ACTION"(
    "ID" INT DEFAULT NEXT VALUE FOR "PUBLIC"."SYSTEM_SEQUENCE_F490330D_9908_48AC_856B_60379B07C29C" NOT NULL NULL_TO_DEFAULT SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_F490330D_9908_48AC_856B_60379B07C29C",
    "IDENTITY" VARCHAR2(4096) NOT NULL,
    "SOURCE_ID" VARCHAR2(100) NOT NULL,
    "SOURCE_NAME" VARCHAR2(1000) NOT NULL,
    "SOURCE_TYPE" VARCHAR2(1000) NOT NULL,
    "OPERATION" VARCHAR2(50) NOT NULL,
    "ACTION_TIMESTAMP" TIMESTAMP NOT NULL
);        
ALTER TABLE "PUBLIC"."ACTION" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_7" PRIMARY KEY("ID");        
-- 86 +/- SELECT COUNT(*) FROM PUBLIC.ACTION;  
INSERT INTO "PUBLIC"."ACTION" VALUES
(1, 'anonymous', '8ae3baa9-0180-1000-0000-00004bece00b', 'AttributeRollingWindow', 'Processor', 'Add', TIMESTAMP '2022-05-03 17:07:02.048'),
(2, 'anonymous', '8ae3baa9-0180-1000-0000-00004bece00b', 'AttributeRollingWindow', 'Processor', 'Remove', TIMESTAMP '2022-05-03 17:08:29.9'),
(3, 'anonymous', '66ef1ea4-7925-398d-8c2a-4e0091bd2f69', 'AttributeRollingWindow', 'Processor', 'Add', TIMESTAMP '2022-05-03 17:09:07.043'),
(4, 'anonymous', '66ef1ea4-7925-398d-8c2a-4e0091bd2f69', 'AttributeRollingWindow', 'Processor', 'Remove', TIMESTAMP '2022-05-03 17:17:56.134'),
(5, 'anonymous', '8aeddfa2-0180-1000-ffff-fffff2cb0395', 'GetFile', 'Processor', 'Add', TIMESTAMP '2022-05-03 17:18:06.769'),
(6, 'anonymous', '8aeddfa2-0180-1000-ffff-fffff2cb0395', 'GetDogeCoinData', 'Processor', 'Configure', TIMESTAMP '2022-05-03 17:20:01.695'),
(7, 'anonymous', '8aeddfa2-0180-1000-ffff-fffff2cb0395', 'GetDogeCoinData', 'Processor', 'Configure', TIMESTAMP '2022-05-03 17:20:01.695'),
(8, 'anonymous', '8aeddfa2-0180-1000-ffff-fffff2cb0395', 'GetDogeCoinData', 'Processor', 'Configure', TIMESTAMP '2022-05-03 17:20:01.695'),
(9, 'anonymous', '8aeddfa2-0180-1000-ffff-fffff2cb0395', 'GetDogeCoinData', 'Processor', 'Configure', TIMESTAMP '2022-05-03 17:20:01.695'),
(10, 'anonymous', '8aeddfa2-0180-1000-ffff-fffff2cb0395', 'GetDogeCoinData', 'Processor', 'Configure', TIMESTAMP '2022-05-03 17:20:01.695'),
(11, 'anonymous', '8af0483d-0180-1000-0000-00001359ea06', 'SplitRecord', 'Processor', 'Add', TIMESTAMP '2022-05-03 17:20:44.621'),
(12, 'anonymous', '8af11c31-0180-1000-ffff-ffffabdb9a4b', 'CSVReader', 'ControllerService', 'Add', TIMESTAMP '2022-05-03 17:21:39.036'),
(13, 'anonymous', '8af14500-0180-1000-ffff-ffff8026e6af', 'CSVRecordSetWriter', 'ControllerService', 'Add', TIMESTAMP '2022-05-03 17:21:49.328'),
(14, 'anonymous', '8af0483d-0180-1000-0000-00001359ea06', 'SplitDogeCoinData', 'Processor', 'Configure', TIMESTAMP '2022-05-03 17:21:54.985'),
(15, 'anonymous', '8af0483d-0180-1000-0000-00001359ea06', 'SplitDogeCoinData', 'Processor', 'Configure', TIMESTAMP '2022-05-03 17:21:54.985'),
(16, 'anonymous', '8af0483d-0180-1000-0000-00001359ea06', 'SplitDogeCoinData', 'Processor', 'Configure', TIMESTAMP '2022-05-03 17:21:54.985'),
(17, 'anonymous', '8af0483d-0180-1000-0000-00001359ea06', 'SplitDogeCoinData', 'Processor', 'Configure', TIMESTAMP '2022-05-03 17:21:54.985'),
(18, 'anonymous', '8af0483d-0180-1000-0000-00001359ea06', 'SplitDogeCoinData', 'Processor', 'Configure', TIMESTAMP '2022-05-03 17:21:54.985'),
(19, 'anonymous', '8af1788d-0180-1000-ffff-ffffd020466b', 'success', 'Connection', 'Connect', TIMESTAMP '2022-05-03 17:22:02.582'),
(20, 'anonymous', '8af1b4e2-0180-1000-0000-00004f453684', 'DogeCoin', 'ProcessGroup', 'Add', TIMESTAMP '2022-05-03 17:22:17.976'),
(21, 'anonymous', '3b453dff-e853-13e2-a22a-02793179f138', 'GetDogeCoinData', 'Processor', 'Add', TIMESTAMP '2022-05-03 17:22:38.142'),
(22, 'anonymous', 'eddd3a19-2624-1b5c-a471-d0ff6d1d2d99', 'SplitDogeCoinData', 'Processor', 'Add', TIMESTAMP '2022-05-03 17:22:38.142'),
(23, 'anonymous', '8af21c51-0180-1000-0000-0000551a44a1', 'success', 'Connection', 'Connect', TIMESTAMP '2022-05-03 17:22:44.453'),
(24, 'anonymous', '8aeddfa2-0180-1000-ffff-fffff2cb0395', 'GetDogeCoinData', 'Processor', 'Remove', TIMESTAMP '2022-05-03 17:22:50.363'),
(25, 'anonymous', '8af0483d-0180-1000-0000-00001359ea06', 'SplitDogeCoinData', 'Processor', 'Remove', TIMESTAMP '2022-05-03 17:22:51.371'),
(26, 'anonymous', '8af279b8-0180-1000-ffff-ffffd56eba5e', 'Funnel', 'Funnel', 'Add', TIMESTAMP '2022-05-03 17:23:08.378'),
(27, 'anonymous', '8af282e8-0180-1000-0000-0000612b359a', 'Funnel', 'Funnel', 'Add', TIMESTAMP '2022-05-03 17:23:10.709'),
(28, 'anonymous', '8af297f1-0180-1000-ffff-ffff9897b228', 'splits', 'Connection', 'Connect', TIMESTAMP '2022-05-03 17:23:16.094'),
(29, 'anonymous', '8af2ab40-0180-1000-ffff-ffffa60ae4fa', 'failure, original', 'Connection', 'Connect', TIMESTAMP '2022-05-03 17:23:21.037'),
(30, 'anonymous', '8af11c31-0180-1000-ffff-ffffabdb9a4b', 'CSVReader', 'ControllerService', 'Configure', TIMESTAMP '2022-05-03 17:23:46.231');        
INSERT INTO "PUBLIC"."ACTION" VALUES
(31, 'anonymous', '8af11c31-0180-1000-ffff-ffffabdb9a4b', 'CSVReader', 'ControllerService', 'Enable', TIMESTAMP '2022-05-03 17:23:48.793'),
(32, 'anonymous', '8af14500-0180-1000-ffff-ffff8026e6af', 'CSVRecordSetWriter', 'ControllerService', 'Enable', TIMESTAMP '2022-05-03 17:23:54.515'),
(33, 'anonymous', '9f1b376a-3893-14a0-9a5c-a965c5c38ab2', 'Copy of DogeCoin', 'ProcessGroup', 'Add', TIMESTAMP '2022-05-03 17:25:24.977'),
(34, 'anonymous', '9f1b376a-3893-14a0-9a5c-a965c5c38ab2', 'Tesla', 'ProcessGroup', 'Configure', TIMESTAMP '2022-05-03 17:26:02.799'),
(35, 'anonymous', 'd7003131-053f-11df-b955-e55c6a9c9914', 'splits', 'Connection', 'Disconnect', TIMESTAMP '2022-05-03 17:26:53.966'),
(36, 'anonymous', 'ac603d85-8d50-191b-b19c-de5ef884f76f', 'failure, original', 'Connection', 'Disconnect', TIMESTAMP '2022-05-03 17:26:54.851'),
(37, 'anonymous', '56a33d5d-373b-1a26-b82d-6c8880203c6b', 'Funnel', 'Funnel', 'Remove', TIMESTAMP '2022-05-03 17:26:55.702'),
(38, 'anonymous', '843a3082-130f-176c-a817-c2f77d459e26', 'Funnel', 'Funnel', 'Remove', TIMESTAMP '2022-05-03 17:26:56.85'),
(39, 'anonymous', 'f2903240-8a9e-1a76-8a7f-14c9a5ed21a4', 'success', 'Connection', 'Disconnect', TIMESTAMP '2022-05-03 17:27:00.505'),
(40, 'anonymous', '71143ceb-e7fd-117d-86c8-9a537508c44a', 'SplitDogeCoinData', 'Processor', 'Remove', TIMESTAMP '2022-05-03 17:27:01.586'),
(41, 'anonymous', 'd700316c-053f-11df-0000-0000215fe9ba', 'Funnel', 'Funnel', 'Add', TIMESTAMP '2022-05-03 17:27:22.004'),
(42, 'anonymous', 'd700316c-053f-11df-0000-0000215fe9ba', 'Funnel', 'Funnel', 'Remove', TIMESTAMP '2022-05-03 17:27:30.653'),
(43, 'anonymous', 'd7003178-053f-11df-ffff-ffff8200e5b6', 'Tesla_DataSet', 'OutputPort', 'Add', TIMESTAMP '2022-05-03 17:27:51.26'),
(44, 'anonymous', 'd700317a-053f-11df-ffff-ffffb98b5d51', 'success', 'Connection', 'Connect', TIMESTAMP '2022-05-03 17:27:55.313'),
(45, 'anonymous', 'd7003184-053f-11df-ffff-ffffae6a322d', 'Funnel', 'Funnel', 'Add', TIMESTAMP '2022-05-03 17:28:02.66'),
(46, 'anonymous', 'd7003187-053f-11df-0000-00003bfb24b6', '', 'Connection', 'Connect', TIMESTAMP '2022-05-03 17:28:06.742'),
(47, 'anonymous', '8af297f1-0180-1000-ffff-ffff9897b228', 'splits', 'Connection', 'Disconnect', TIMESTAMP '2022-05-03 17:29:06.76'),
(48, 'anonymous', '8af282e8-0180-1000-0000-0000612b359a', 'Funnel', 'Funnel', 'Remove', TIMESTAMP '2022-05-03 17:29:07.76'),
(49, 'anonymous', 'd70031b6-053f-11df-ffff-ffffdd3b15ce', 'DogeCoinStreaming', 'OutputPort', 'Add', TIMESTAMP '2022-05-03 17:29:34.202'),
(50, 'anonymous', 'd70031b8-053f-11df-0000-0000234796d2', 'splits', 'Connection', 'Connect', TIMESTAMP '2022-05-03 17:29:41.066'),
(51, 'anonymous', 'd70031d3-053f-11df-ffff-fffffdc87f69', 'Funnel', 'Funnel', 'Add', TIMESTAMP '2022-05-03 17:30:20.481'),
(52, 'anonymous', 'd70031d6-053f-11df-0000-000002409dd4', '', 'Connection', 'Connect', TIMESTAMP '2022-05-03 17:30:24.623'),
(53, 'anonymous', 'd7003313-053f-11df-0000-0000644a240a', 'PublishKafkaRecord_2_6', 'Processor', 'Add', TIMESTAMP '2022-05-03 17:56:47.364'),
(54, 'anonymous', 'd70031d6-053f-11df-0000-000002409dd4', '', 'Connection', 'Disconnect', TIMESTAMP '2022-05-03 18:27:02.594'),
(55, 'anonymous', 'd70031d3-053f-11df-ffff-fffffdc87f69', 'Funnel', 'Funnel', 'Remove', TIMESTAMP '2022-05-03 18:27:04.133'),
(56, 'anonymous', 'd7003416-053f-11df-0000-00005b92d302', '', 'Connection', 'Connect', TIMESTAMP '2022-05-03 18:27:13.973'),
(57, 'anonymous', 'd7003418-053f-11df-0000-00004376dcdf', 'Funnel', 'Funnel', 'Add', TIMESTAMP '2022-05-03 18:27:20.115'),
(58, 'anonymous', 'd700341e-053f-11df-ffff-ffffa2c5749a', 'Funnel', 'Funnel', 'Add', TIMESTAMP '2022-05-03 18:27:24.04'),
(59, 'anonymous', 'd7003420-053f-11df-0000-00004e60582d', 'success', 'Connection', 'Connect', TIMESTAMP '2022-05-03 18:27:28.287'),
(60, 'anonymous', 'd7003423-053f-11df-0000-0000531d63bd', 'failure', 'Connection', 'Connect', TIMESTAMP '2022-05-03 18:27:31.867'),
(61, 'anonymous', 'd7003443-053f-11df-ffff-fffff23893d1', 'PublishKafka_1_0', 'Processor', 'Add', TIMESTAMP '2022-05-03 18:30:51.269');      
INSERT INTO "PUBLIC"."ACTION" VALUES
(62, 'anonymous', 'd700344a-053f-11df-0000-00000cc94477', 'PublishKafka_2_6', 'Processor', 'Add', TIMESTAMP '2022-05-03 18:31:24.888'),
(63, 'anonymous', 'd7003443-053f-11df-ffff-fffff23893d1', 'PublishKafka_1_0', 'Processor', 'Remove', TIMESTAMP '2022-05-03 18:31:28.4'),
(64, 'anonymous', 'd7003423-053f-11df-0000-0000531d63bd', 'failure', 'Connection', 'Disconnect', TIMESTAMP '2022-05-03 18:31:33.455'),
(65, 'anonymous', 'd7003420-053f-11df-0000-00004e60582d', 'success', 'Connection', 'Disconnect', TIMESTAMP '2022-05-03 18:31:34.598'),
(66, 'anonymous', 'd7003416-053f-11df-0000-00005b92d302', '', 'Connection', 'Disconnect', TIMESTAMP '2022-05-03 18:31:37.44'),
(67, 'anonymous', 'd7003313-053f-11df-0000-0000644a240a', 'PublishKafkaRecord_2_6', 'Processor', 'Remove', TIMESTAMP '2022-05-03 18:31:38.696'),
(68, 'anonymous', 'd700345d-053f-11df-0000-00006251da2e', '', 'Connection', 'Connect', TIMESTAMP '2022-05-03 18:31:45.729'),
(69, 'anonymous', 'd7003460-053f-11df-0000-00004a091937', 'failure', 'Connection', 'Connect', TIMESTAMP '2022-05-03 18:31:51.342'),
(70, 'anonymous', 'd7003463-053f-11df-0000-00006d231d27', 'success', 'Connection', 'Connect', TIMESTAMP '2022-05-03 18:31:54.899'),
(71, 'anonymous', 'd700344a-053f-11df-0000-00000cc94477', 'PublishKafka_2_6', 'Processor', 'Configure', TIMESTAMP '2022-05-03 18:38:25.71'),
(72, 'anonymous', 'd700344a-053f-11df-0000-00000cc94477', 'PublishKafka_2_6', 'Processor', 'Configure', TIMESTAMP '2022-05-03 18:38:25.71'),
(73, 'anonymous', 'd700344a-053f-11df-0000-00000cc94477', 'PublishKafka_2_6', 'Processor', 'Configure', TIMESTAMP '2022-05-03 18:38:25.71'),
(74, 'anonymous', 'd700344a-053f-11df-0000-00000cc94477', 'PublishKafka_2_6', 'Processor', 'Configure', TIMESTAMP '2022-05-03 18:38:52.831'),
(75, 'anonymous', 'd700344a-053f-11df-0000-00000cc94477', 'PublishKafka_2_6', 'Processor', 'Start', TIMESTAMP '2022-05-03 18:40:48.185'),
(76, 'anonymous', 'd700344a-053f-11df-0000-00000cc94477', 'PublishKafka_2_6', 'Processor', 'Stop', TIMESTAMP '2022-05-03 18:40:52.111'),
(97, 'anonymous', '29c40a2d-cf0c-36ee-9dcc-899f07783ff0', 'Tesla', 'ProcessGroup', 'Add', TIMESTAMP '2022-05-03 19:11:05.415'),
(98, 'anonymous', 'ea7c5a0c-c717-34ee-8b5d-601bc8ed61a2', 'DogeCoin', 'ProcessGroup', 'Add', TIMESTAMP '2022-05-03 19:11:05.415'),
(99, 'anonymous', '6f76ebf8-ba3f-3af6-ab8a-032636ba84ee', 'PublishKafka_2_6', 'Processor', 'Add', TIMESTAMP '2022-05-03 19:11:05.415'),
(100, 'anonymous', '04eb5d8c-a868-3d07-a81f-c65cbde22433', '', 'Funnel', 'Add', TIMESTAMP '2022-05-03 19:11:05.415'),
(101, 'anonymous', '5fd7acf8-e2e8-3bac-8d37-e5fb4c28d3f7', '', 'Funnel', 'Add', TIMESTAMP '2022-05-03 19:11:05.415'),
(102, 'anonymous', 'f2563950-3e71-33e0-a20a-36aaf77a0b78', '', 'Funnel', 'Add', TIMESTAMP '2022-05-03 19:11:05.415'),
(103, 'anonymous', '58feb270-ee54-3e4f-a226-bfe4095ce4c3', 'success', 'Connection', 'Connect', TIMESTAMP '2022-05-03 19:11:05.415'),
(104, 'anonymous', '61ff0f36-88c8-3c05-bb35-dbdee8db12b1', '', 'Connection', 'Connect', TIMESTAMP '2022-05-03 19:11:05.415'),
(105, 'anonymous', '754b78bb-bb2b-3761-8d2c-62aa200aa202', '', 'Connection', 'Connect', TIMESTAMP '2022-05-03 19:11:05.415'),
(106, 'anonymous', '93a8254d-3d7b-33bf-a8fe-4502c667549f', 'failure', 'Connection', 'Connect', TIMESTAMP '2022-05-03 19:11:05.415');            
CREATE CACHED TABLE "PUBLIC"."PROCESSOR_DETAILS"(
    "ACTION_ID" INT NOT NULL,
    "TYPE" VARCHAR2(1000) NOT NULL
);          
ALTER TABLE "PUBLIC"."PROCESSOR_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_5" PRIMARY KEY("ACTION_ID");      
-- 38 +/- SELECT COUNT(*) FROM PUBLIC.PROCESSOR_DETAILS;       
INSERT INTO "PUBLIC"."PROCESSOR_DETAILS" VALUES
(1, 'AttributeRollingWindow'),
(2, 'AttributeRollingWindow'),
(3, 'AttributeRollingWindow'),
(4, 'AttributeRollingWindow'),
(5, 'GetFile'),
(6, 'GetFile'),
(7, 'GetFile'),
(8, 'GetFile'),
(9, 'GetFile'),
(10, 'GetFile'),
(11, 'SplitRecord'),
(12, 'CSVReader'),
(13, 'CSVRecordSetWriter'),
(14, 'SplitRecord'),
(15, 'SplitRecord'),
(16, 'SplitRecord'),
(17, 'SplitRecord'),
(18, 'SplitRecord'),
(21, 'GetFile'),
(22, 'SplitRecord'),
(24, 'GetFile'),
(25, 'SplitRecord'),
(30, 'CSVReader'),
(31, 'CSVReader'),
(32, 'CSVRecordSetWriter'),
(40, 'SplitRecord'),
(53, 'PublishKafkaRecord_2_6'),
(61, 'PublishKafka_1_0'),
(62, 'PublishKafka_2_6'),
(63, 'PublishKafka_1_0'),
(67, 'PublishKafkaRecord_2_6'),
(71, 'PublishKafka_2_6'),
(72, 'PublishKafka_2_6'),
(73, 'PublishKafka_2_6'),
(74, 'PublishKafka_2_6'),
(75, 'PublishKafka_2_6'),
(76, 'PublishKafka_2_6'),
(99, 'PublishKafka_2_6');               
CREATE CACHED TABLE "PUBLIC"."REMOTE_PROCESS_GROUP_DETAILS"(
    "ACTION_ID" INT NOT NULL,
    "URI" VARCHAR2(2500) NOT NULL
);
ALTER TABLE "PUBLIC"."REMOTE_PROCESS_GROUP_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_7F" PRIMARY KEY("ACTION_ID");          
-- 0 +/- SELECT COUNT(*) FROM PUBLIC.REMOTE_PROCESS_GROUP_DETAILS;             
CREATE CACHED TABLE "PUBLIC"."MOVE_DETAILS"(
    "ACTION_ID" INT NOT NULL,
    "GROUP_ID" VARCHAR2(100) NOT NULL,
    "GROUP_NAME" VARCHAR2(1000) NOT NULL,
    "PREVIOUS_GROUP_ID" VARCHAR2(100) NOT NULL,
    "PREVIOUS_GROUP_NAME" VARCHAR2(1000) NOT NULL
);               
ALTER TABLE "PUBLIC"."MOVE_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_5E" PRIMARY KEY("ACTION_ID");          
-- 0 +/- SELECT COUNT(*) FROM PUBLIC.MOVE_DETAILS;             
CREATE CACHED TABLE "PUBLIC"."CONFIGURE_DETAILS"(
    "ACTION_ID" INT NOT NULL,
    "NAME" VARCHAR2(1000) NOT NULL,
    "VALUE" VARCHAR2(5000),
    "PREVIOUS_VALUE" VARCHAR2(5000)
);         
ALTER TABLE "PUBLIC"."CONFIGURE_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_B" PRIMARY KEY("ACTION_ID");      
-- 16 +/- SELECT COUNT(*) FROM PUBLIC.CONFIGURE_DETAILS;       
INSERT INTO "PUBLIC"."CONFIGURE_DETAILS" VALUES
(6, 'Keep Source File', 'true', 'false'),
(7, 'Run Schedule', '60 sec', '0 sec'),
(8, 'File Filter', 'DOGE-USD\.csv', '[^\.].*'),
(9, 'Name', 'GetDogeCoinData', 'GetFile'),
(10, 'Input Directory', '/home/nifi', NULL),
(14, 'Run Schedule', '60 sec', '0 sec'),
(15, 'Record Reader', '8af11c31-0180-1000-ffff-ffffabdb9a4b', NULL),
(16, 'Name', 'SplitDogeCoinData', 'SplitRecord'),
(17, 'Record Writer', '8af14500-0180-1000-ffff-ffff8026e6af', NULL),
(18, 'Records Per Split', '1', NULL),
(30, 'Comments', '', NULL),
(34, 'Name', 'Tesla', 'Copy of DogeCoin'),
(71, 'acks', '1', '0'),
(72, 'bootstrap.servers', 'localhost:29092', 'localhost:9092'),
(73, 'topic', 'topic-kafka', NULL),
(74, 'acks', 'all', '1');              
CREATE CACHED TABLE "PUBLIC"."CONNECT_DETAILS"(
    "ACTION_ID" INT NOT NULL,
    "SOURCE_ID" VARCHAR2(100) NOT NULL,
    "SOURCE_NAME" VARCHAR2(1000),
    "SOURCE_TYPE" VARCHAR2(1000) NOT NULL,
    "RELATIONSHIP" VARCHAR2(1000),
    "DESTINATION_ID" VARCHAR2(100) NOT NULL,
    "DESTINATION_NAME" VARCHAR2(1000),
    "DESTINATION_TYPE" VARCHAR2(1000) NOT NULL
);    
ALTER TABLE "PUBLIC"."CONNECT_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_76" PRIMARY KEY("ACTION_ID");       
-- 26 +/- SELECT COUNT(*) FROM PUBLIC.CONNECT_DETAILS;         
INSERT INTO "PUBLIC"."CONNECT_DETAILS" VALUES
(19, '8aeddfa2-0180-1000-ffff-fffff2cb0395', 'GetDogeCoinData', 'Processor', 'success', '8af0483d-0180-1000-0000-00001359ea06', 'SplitDogeCoinData', 'Processor'),
(23, '3b453dff-e853-13e2-a22a-02793179f138', 'GetDogeCoinData', 'Processor', 'success', 'eddd3a19-2624-1b5c-a471-d0ff6d1d2d99', 'SplitDogeCoinData', 'Processor'),
(28, 'eddd3a19-2624-1b5c-a471-d0ff6d1d2d99', 'SplitDogeCoinData', 'Processor', 'splits', '8af282e8-0180-1000-0000-0000612b359a', 'Funnel', 'Funnel'),
(29, 'eddd3a19-2624-1b5c-a471-d0ff6d1d2d99', 'SplitDogeCoinData', 'Processor', 'failure, original', '8af279b8-0180-1000-ffff-ffffd56eba5e', 'Funnel', 'Funnel'),
(35, '71143ceb-e7fd-117d-86c8-9a537508c44a', 'SplitDogeCoinData', 'Processor', 'splits', '843a3082-130f-176c-a817-c2f77d459e26', 'Funnel', 'Funnel'),
(36, '71143ceb-e7fd-117d-86c8-9a537508c44a', 'SplitDogeCoinData', 'Processor', 'failure, original', '56a33d5d-373b-1a26-b82d-6c8880203c6b', 'Funnel', 'Funnel'),
(39, '463a373d-3aeb-1f92-b664-03c218460dc9', 'GetDogeCoinData', 'Processor', 'success', '71143ceb-e7fd-117d-86c8-9a537508c44a', 'SplitDogeCoinData', 'Processor'),
(44, '463a373d-3aeb-1f92-b664-03c218460dc9', 'GetDogeCoinData', 'Processor', 'success', 'd7003178-053f-11df-ffff-ffff8200e5b6', 'Tesla_DataSet', 'OutputPort'),
(46, 'd7003178-053f-11df-ffff-ffff8200e5b6', 'Tesla_DataSet', 'OutputPort', '', 'd7003184-053f-11df-ffff-ffffae6a322d', 'Funnel', 'Funnel'),
(47, 'eddd3a19-2624-1b5c-a471-d0ff6d1d2d99', 'SplitDogeCoinData', 'Processor', 'splits', '8af282e8-0180-1000-0000-0000612b359a', 'Funnel', 'Funnel'),
(50, 'eddd3a19-2624-1b5c-a471-d0ff6d1d2d99', 'SplitDogeCoinData', 'Processor', 'splits', 'd70031b6-053f-11df-ffff-ffffdd3b15ce', 'DogeCoinStreaming', 'OutputPort'),
(52, 'd70031b6-053f-11df-ffff-ffffdd3b15ce', 'DogeCoinStreaming', 'OutputPort', '', 'd70031d3-053f-11df-ffff-fffffdc87f69', 'Funnel', 'Funnel'),
(54, 'd70031b6-053f-11df-ffff-ffffdd3b15ce', 'DogeCoinStreaming', 'OutputPort', '', 'd70031d3-053f-11df-ffff-fffffdc87f69', 'Funnel', 'Funnel'),
(56, 'd70031b6-053f-11df-ffff-ffffdd3b15ce', 'DogeCoinStreaming', 'OutputPort', '', 'd7003313-053f-11df-0000-0000644a240a', 'PublishKafkaRecord_2_6', 'Processor'),
(59, 'd7003313-053f-11df-0000-0000644a240a', 'PublishKafkaRecord_2_6', 'Processor', 'success', 'd700341e-053f-11df-ffff-ffffa2c5749a', 'Funnel', 'Funnel'),
(60, 'd7003313-053f-11df-0000-0000644a240a', 'PublishKafkaRecord_2_6', 'Processor', 'failure', 'd7003418-053f-11df-0000-00004376dcdf', 'Funnel', 'Funnel'),
(64, 'd7003313-053f-11df-0000-0000644a240a', 'PublishKafkaRecord_2_6', 'Processor', 'failure', 'd7003418-053f-11df-0000-00004376dcdf', 'Funnel', 'Funnel'),
(65, 'd7003313-053f-11df-0000-0000644a240a', 'PublishKafkaRecord_2_6', 'Processor', 'success', 'd700341e-053f-11df-ffff-ffffa2c5749a', 'Funnel', 'Funnel'),
(66, 'd70031b6-053f-11df-ffff-ffffdd3b15ce', 'DogeCoinStreaming', 'OutputPort', '', 'd7003313-053f-11df-0000-0000644a240a', 'PublishKafkaRecord_2_6', 'Processor'),
(68, 'd70031b6-053f-11df-ffff-ffffdd3b15ce', 'DogeCoinStreaming', 'OutputPort', '', 'd700344a-053f-11df-0000-00000cc94477', 'PublishKafka_2_6', 'Processor'),
(69, 'd700344a-053f-11df-0000-00000cc94477', 'PublishKafka_2_6', 'Processor', 'failure', 'd7003418-053f-11df-0000-00004376dcdf', 'Funnel', 'Funnel'),
(70, 'd700344a-053f-11df-0000-00000cc94477', 'PublishKafka_2_6', 'Processor', 'success', 'd700341e-053f-11df-ffff-ffffa2c5749a', 'Funnel', 'Funnel'),
(103, '6f76ebf8-ba3f-3af6-ab8a-032636ba84ee', 'PublishKafka_2_6', 'Processor', 'success', '5fd7acf8-e2e8-3bac-8d37-e5fb4c28d3f7', NULL, 'Funnel'),
(104, '0fa95015-a983-34a1-996e-51524d0b9d66', 'Tesla_DataSet', 'OutputPort', '', 'f2563950-3e71-33e0-a20a-36aaf77a0b78', NULL, 'Funnel'),
(105, 'db0622ae-d52e-371f-9df2-db06f6daf813', 'DogeCoinStreaming', 'OutputPort', '', '6f76ebf8-ba3f-3af6-ab8a-032636ba84ee', 'PublishKafka_2_6', 'Processor'),
(106, '6f76ebf8-ba3f-3af6-ab8a-032636ba84ee', 'PublishKafka_2_6', 'Processor', 'failure', '04eb5d8c-a868-3d07-a81f-c65cbde22433', NULL, 'Funnel');
CREATE CACHED TABLE "PUBLIC"."PURGE_DETAILS"(
    "ACTION_ID" INT NOT NULL,
    "END_DATE" TIMESTAMP NOT NULL
);               
ALTER TABLE "PUBLIC"."PURGE_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_9" PRIMARY KEY("ACTION_ID");          
-- 0 +/- SELECT COUNT(*) FROM PUBLIC.PURGE_DETAILS;            
ALTER TABLE "PUBLIC"."CONFIGURE_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_BB" FOREIGN KEY("ACTION_ID") REFERENCES "PUBLIC"."ACTION"("ID") NOCHECK;          
ALTER TABLE "PUBLIC"."PROCESSOR_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_50" FOREIGN KEY("ACTION_ID") REFERENCES "PUBLIC"."ACTION"("ID") NOCHECK;          
ALTER TABLE "PUBLIC"."PURGE_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_93" FOREIGN KEY("ACTION_ID") REFERENCES "PUBLIC"."ACTION"("ID") NOCHECK;              
ALTER TABLE "PUBLIC"."CONNECT_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_76F" FOREIGN KEY("ACTION_ID") REFERENCES "PUBLIC"."ACTION"("ID") NOCHECK;           
ALTER TABLE "PUBLIC"."REMOTE_PROCESS_GROUP_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_7F4" FOREIGN KEY("ACTION_ID") REFERENCES "PUBLIC"."ACTION"("ID") NOCHECK;              
ALTER TABLE "PUBLIC"."MOVE_DETAILS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_5E5" FOREIGN KEY("ACTION_ID") REFERENCES "PUBLIC"."ACTION"("ID") NOCHECK;              
